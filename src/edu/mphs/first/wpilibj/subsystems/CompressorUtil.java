/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.mphs.first.wpilibj.commands.CMP_Start;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author marnold
 */
public class CompressorUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    Compressor comp;
   
    public CompressorUtil(){
        comp = new Compressor(RobotMap.cmp_SWITCH, RobotMap.cmp_RELAY);
    }
    
    public void enable(){
        comp.start();
    }
    
    public boolean isEnabled(){
        return comp.enabled();
    }
    
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        if(!isEnabled()){
            setDefaultCommand(new CMP_Start());            
        }
    }
    
    public void updateStatus(){
        //SmartDashboard.putBoolean("Compressor Enabled", comp.enabled());
    }
}
