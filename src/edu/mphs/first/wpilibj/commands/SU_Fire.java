/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;
import edu.wpi.first.wpilibj.Timer;

/**
 *
 * @author marnold
 */
public class SU_Fire extends CommandBase {
    boolean done;
    
    public SU_Fire() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(shotUtil);
        this.done = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        if(oi.getFireControl()){
            oi.setFireControl(false);
            shotUtil.fire();
            Timer.delay(RobotMap.su_TIMER_DELAY);
        }
        this.done = true;

    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return this.done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
    

}
