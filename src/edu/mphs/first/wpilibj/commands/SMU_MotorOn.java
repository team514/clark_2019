/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author marnold
 */
public class SMU_MotorOn extends CommandBase {
    
    public SMU_MotorOn() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(motorUtil);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        motorUtil.setMotor1(1.0);
        motorUtil.setMotor2(1.0);
        oi.setShotMotors(true);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
