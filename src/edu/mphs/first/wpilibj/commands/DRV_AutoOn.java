/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.wpi.first.wpilibj.Timer;

/**
 *
 * @author Team 514
 */
public class DRV_AutoOn extends CommandBase {
    int MyMode;
    boolean done;
    
    public DRV_AutoOn(int mode) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(driveUtil);
        this.MyMode = mode;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        if(MyMode == 2){
            driveUtil.driveTank(-.65, -.65);
        }
        if(MyMode == 3){
            driveUtil.driveTank(-.50, -.50);    
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
