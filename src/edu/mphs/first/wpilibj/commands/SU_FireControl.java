/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author marnold
 */
public class SU_FireControl extends CommandBase {
    boolean status;
    public SU_FireControl() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(shotUtil);
        this.status = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        shotUtil.dontfire();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        //manageMag
            this.status = false;
            oi.setMagControl(shotUtil.getMagSensor());
            oi.setChamberStatus(shotUtil.getChamber());
        //Manage the loading of the frizbees and set fire control status
        if(shotUtil.getPin()){
            if(shotUtil.getChamber()){
                this.status = true;                
                shotUtil.closeGate();
            }else{
                this.status = false;
                shotUtil.openGate();
            }
        //Safety Check - make sure Shot Motors are On before Fireing!
            if(oi.getShotMotors()){
                oi.setFireControl(this.status);
            }else{
                oi.setFireControl(false);
            }
            
        }else{
            this.status = false;
            oi.setFireControl(false);
            shotUtil.closeGate();
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return oi.getFireControl();
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
