/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author marnold
 */
public class SMU_ManageMotor extends CommandBase {
    
    public SMU_ManageMotor() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(motorUtil);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        if(oi.getChamberStatus()){
           motorUtil.setMotor1(1.0);
           motorUtil.setMotor2(1.0);
           oi.setShotMotors(true);
        }else{
            if(oi.getMagControl()){
               motorUtil.setMotor1(1.0);
               motorUtil.setMotor2(1.0);
               oi.setShotMotors(true);    
            }else{
                motorUtil.setMotor1(0.00);
                motorUtil.setMotor2(0.00);
                oi.setShotMotors(false);                
            }
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
